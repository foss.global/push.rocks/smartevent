import * as plugins from './smartevent.plugins';

import { EventEmitter } from 'events';

export {
  
};

export class SmartEventEmitter {
  public eventSubject = new plugins.

  public once () {

  };

  public emit () {

  }

  public on () {}
}

// instrument globalThis

export const once = async <T>(eventEmitter: SmartEventEmitter, eventName: string): Promise<T> => {
  const done = plugins.smartpromise.defer<T>();
  eventEmitter.once(eventName, eventPayload => {
    done.resolve(eventPayload);
  });
  return await done.promise;
};
